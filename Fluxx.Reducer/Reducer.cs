﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Tracing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using Fluxx.Core;

namespace Fluxx.Reducer
{
	public class Reducer : TreeReducer<Node>
	{

		#region "private fields"
		
		//functions routes
		//private readonly Dictionary<NodeType, Func<Node, Node>> _funcs;
		//keep root node in memory
		private Node _root;
		
		#endregion

		//public Reducer() : base() {}
		
		public Node StartReduce(Node root)
		{
			_root = root;
			
			/*
			//add builtins
			foreach (var kvRef in _refs)
			{
				Node n = new Node(NodeType.CONTAINER, root);
				n.Value = kvRef.Value;
				root.Context.AddVar(kvRef.Key, n);
			}
			*/
			
			

			return Reduce(root);
		}

		public Node ReduceBuiltin(Node n)
		{
			Func<Node, Node> f = (Func<Node, Node>)n.Value;
			return f(n);
		}

		public Node ReduceBuiltin2(Node n)
		{
			if (!(n.Value is Delegate))
			{
				Console.WriteLine("invalid context transfer");
				//throw new Exception("invalid context transfer");
			}

			

			/*
			Func<object[], object> f = (Func<object[], object>) n.Value;
			Node rn = new Node(n);
			object[] arguments = n.Context.GetExpressions().Select(e => e.Value).ToArray();
			object result = f.Invoke(arguments);
			Type typeResult = result.GetType();

			rn.Value = result;
			
			Type vType = rn.Value.GetType();
			if (vType == typeof(double))
				rn.Type = NodeType.NUMBER;
			else if (vType == typeof(string))
				rn.Type = NodeType.STRING;
			
			return rn;
			*/

			var f = (Func<Node, Node>) n.Value;
			return f(n.ReceivedNode);

		}

		public override Node Parenthesis(Node n)
		{
			return Reduce(n.Children.First());
		}

		public override Node Container(Node n)
		{
			
			Node rn1 = ReduceContainer1Pass(n);
			Node rn2 = ReduceContainer2Pass(rn1);
			Node rn3 = ReduceContainer3Pass(rn2);

			if (rn3.Value != null)
			{
				return ReduceBuiltin2(rn3);

			}

			return rn3;
		}

		public Node ReduceContainer1Pass(Node n)
		{
			/*
			Node rn = n.DataCopy();

			foreach (var child in n.Children)
			{
				if (child.Type == NodeType.BIND)
				{
					ReduceBind(child, rn);
				}
				else 
					rn.Add(child);
			}

			return rn;
			*/
			return n;
		}

		public Node ReduceContainer2Pass(Node n)
		{
			Node rn = n.DataCopy();
			/*
			if (rn.Parent == null)
			{
				Parallel.ForEach(n.Children, (child) =>
				{
					if (child.Type != NodeType.BIND)
						rn.Add(Reduce(child));
					else
						rn.Add(child);
				});
			}
			else
			{*/
				foreach (var child in n.Children)
				{
					if (child.Type != NodeType.Bind)
						rn.Add(Reduce(child));
					else
						rn.Add(child);

				}
			/*}*/

			return rn;
		}

		public Node ReduceContainer3Pass(Node n)
		{
			Node rn = n.DataCopy();

			Parallel.ForEach(n.Children, (child) =>
			{
				if (child.Type == NodeType.Collection)
					rn.AddRange(child.Children);
				else
					rn.Add(child);
			});

			return rn;
		}

		public Node Resolve(Node n)
		{
			Node rn = n.ShallowCopy();
			
			while (rn.Type == NodeType.Id || rn.Type == NodeType.Param)
			{
				if (rn.Type == NodeType.Id)
					rn = GetIdValue(rn);
				else
					rn = GetParamValue(rn);
			}

			return rn;
		}
		
		public override Node Blank(Node n)
		{
			var l = n[0];
			var r = n[1];
			
			var lr = Resolve(l);
			var rr = Reduce(r);
			
			
			
			lr.ReceiveContextFrom(rr);
			
						
			return Reduce(lr);
		}
		
		public override Node Arrow(Node n)
		{
			n.Reverse();
			return Blank(n);
		}

		public Node GetIdValue(Node n)
		{
			string name = n.Value.ToString();

			Node v = null;
			var scope = n.Scope;
			/*
			while (v == null && scope != null)
			{
				v = scope.Context.GetVar(name);
				scope = scope.Scope;
			}
			*/
			
			//Search variable in current scope
			//v = scope.Context.GetVar(name);
			v = scope.GetVar(name);

			//If variable isn't finded get from the root scope (global)
			if (v == null)
				v = _root.GetVar(name);
				//v = _root.Context.GetVar(name);
			
			if (v == null)
				throw new Exception(String.Format("{0} is undefined in this scope", name));

			return v;
		}

		public Node GetParamValue(Node n)
		{
			double dIndex = (double) n.Children.ElementAt(0).Value;
			int index = (int)dIndex;

			if (index < 0)
				throw new Exception("index of expression can't be less than 0");
			
			var scope = n.Scope;
			
			//if (index >= scope.Context.CountExpression())
			if (index >= scope.CountExpression())
				throw new Exception("index of expression is out of bounds");
			
			//return scope.Context.GetExpression(index);
			return scope.GetExpression(index);
		}

		public override Node Id(Node n)
		{
			return Reduce(GetIdValue(n));
		}

		public override Node Param(Node n)
		{
			return Reduce(GetParamValue(n));
		}

		public Node ReduceBind(Node n, Node scope)
		{
			var l = n.Children.ElementAt(0);
			var r = n.Children.ElementAt(1);

			if (l.Type != NodeType.Id)
				throw new Exception("left member should be an id");

			if (r.Type == NodeType.Bind)
				r = ReduceBind(r, scope);


			string name = l.Value.ToString();
			//scope.Context.AddVar(name, r.ShallowCopy());
			scope.AddVar(name, r.ShallowCopy());

			return r;
		}

		public override Node Extract(Node n)
		{
			Node rn = new Node(n.Scope);
			rn.Type = NodeType.Container;
			Node r = Reduce(n.Children.ElementAt(0));
			Node l = Reduce(n.Children.ElementAt(1));
			int index = Convert.ToInt32(l.Value); //la conversion doit être effectué en amont dans le parser

			rn.Add(r.Children.ElementAt(index));

			if (rn.IsUnary)
				return rn.Children.ElementAt(0);
			else
				return rn;
		}

		public override Node Conditional(Node n)
		{
			Node rn = new Node(n.Scope);

			Node nCondition = Reduce(n.Children.ElementAt(0));
			bool val = Convert.ToBoolean(nCondition.Value);


			if (val)
				rn = Reduce(n.Children.ElementAt(1));
			else
				rn = Reduce(n.Children.ElementAt(2));

			return rn;
		}

		public override Node Range(Node n)
		{
			Node rn = new Node(NodeType.Collection, n.Scope);

			Node l = Reduce(n.Children.ElementAt(0));
			Node r = Reduce(n.Children.ElementAt(1));

			double start = Convert.ToInt32(l.Value);
			double end = Convert.ToInt32(r.Value);

			for (double i = start; i < end; i++)
				rn.Add(new Node(NodeType.Number, n.Scope) { Value = i });

			return rn;
		}

		public override Node SetBuilder(Node n)
		{
			
			Node rn = new Node(NodeType.Collection, n.Scope);

			Node exp = n.Children.ElementAt(0);
			Node id = n.Children.ElementAt(1);


			if (id.Type != NodeType.Id)
				throw new Exception("");

			string name = id.Value.ToString();

			Node list = Reduce(n.Children.ElementAt(2));
			//var scope = n.GetScopes().First();
			var scope = n.Scope;
			foreach (var e in list.Children)
			{
				//scope.Context.AddVar(name, e);
				scope.AddVar(name, e);
				Node rExp = Reduce(exp);
				
				if (rExp.Type != NodeType.Collection)
					rn.Add(rExp);
				else
					rn.AddRange(rExp.Children);
				
				//scope.Context.RemoveVar(name);
				scope.RemoveVar(name);
			}


			return rn;
		}

		public override Node Unpack(Node n)
		{
			Node rn = new Node(NodeType.Collection, n.Scope);

			Node child = n[0];

			Node rChild = Resolve(child);
			
			//while (rChild.Type == NodeType.UNPACK)
			
			if (rChild.Type != NodeType.Container)
				throw new Exception(String.Format("unpacking only work on nodes, at {0}", rChild.ToString()));

			Node rrChild = Reduce(rChild);
			
			rn.AddRange(rrChild.Children);
			
			return rn;
		}

		public override Node Collection(Node n)
		{
			return n.Children.Count() == 1 ? n[0] : n;
		}

		public override Node Add(Node n)
		{
			Node rn = new Node(n.Scope);
			rn.Type = NodeType.Number;
			Node l = Reduce(n.Children.ElementAt(0));
			Node r = Reduce(n.Children.ElementAt(1));

			rn.Value = (double)l.Value + (double)r.Value;
			return rn;
		}

		public override Node Sub(Node n)
		{
			Node rn = new Node(n.Scope);
			rn.Type = NodeType.Number;
			Node l = Reduce(n.Children.ElementAt(0));
			Node r = Reduce(n.Children.ElementAt(1));

			rn.Value = (double)l.Value - (double)r.Value;
			return rn;
		}

		public override Node Mul(Node n)
		{
			Node rn = new Node(n.Scope);
			rn.Type = NodeType.Number;
			Node l = Reduce(n.Children.ElementAt(0));
			Node r = Reduce(n.Children.ElementAt(1));

			rn.Value = (double)l.Value * (double)r.Value;
			return rn;
		}
		
		public override Node Mod(Node n)
		{
			Node rn = new Node(n.Scope);
			rn.Type = NodeType.Number;
			Node l = Reduce(n.Children.ElementAt(0));
			Node r = Reduce(n.Children.ElementAt(1));

			rn.Value = (double)l.Value % (double)r.Value;
			return rn;
		}

		public override Node Pow(Node n)
		{
			Node rn = new Node(n.Scope);
			rn.Type = NodeType.Number;
			Node l = Reduce(n.Children.ElementAt(0));
			Node r = Reduce(n.Children.ElementAt(1));

			rn.Value = Math.Pow((double)l.Value, (double)r.Value);
			return rn;
		}

		public override Node Div(Node n)
		{
			Node rn = new Node(n.Scope);
			rn.Type = NodeType.Number;
			Node l = Reduce(n.Children.ElementAt(0));
			Node r = Reduce(n.Children.ElementAt(1));

			rn.Value = (double)l.Value / (double)r.Value;
			return rn;
		}

		public override Node And(Node n)
		{
			Node rn = new Node(n.Scope);
			rn.Type = NodeType.Bool;
			Node l = Reduce(n.Children.ElementAt(0));
			Node r = Reduce(n.Children.ElementAt(1));

			rn.Value = (bool)l.Value && (bool)r.Value;
			return rn;
		}

		public override Node Or(Node n)
		{
			Node rn = new Node(n.Scope);
			rn.Type = NodeType.Bool;
			Node l = Reduce(n.Children.ElementAt(0));
			Node r = Reduce(n.Children.ElementAt(1));

			rn.Value = (bool)l.Value || (bool)r.Value;
			return rn;
		}

		public override Node Not(Node n)
		{
			Node rn = new Node(n.Scope);
			rn.Type = NodeType.Bool;
			Node l = Reduce(n.Children.ElementAt(0));

			rn.Value = !(bool)l.Value;
			return rn;
		}

		public override Node Equal(Node n)
		{
			Node rn = new Node(n.Scope);
			rn.Type = NodeType.Bool;
			Node l = Reduce(n.Children.ElementAt(0));
			Node r = Reduce(n.Children.ElementAt(1));

			rn.Value = l.Value.Equals(r.Value);
			return rn;
		}

		public override Node Minus(Node n)
		{
			Node rn = new Node(n.Scope);

			rn.Type = NodeType.Number;
			rn.Value = -(double)Reduce(n.Children.ElementAt(0)).Value;

			return rn;
		}

		public override Node Plus(Node n)
		{
			Node rn = new Node(n.Scope);

			rn.Type = NodeType.Number;
			rn.Value = Reduce(n.Children.ElementAt(0)).Value;
			
			return rn;
		}

		public override Node Empty(Node n)
			=> n;

		public override Node Number(Node n)
			=> n;
		
		public override Node Bool(Node n)
		{
			return n;
		}
		
		public override Node Str(Node n)
		{
			return n;
		}
		
		public override Node Greater(Node n)
		{
			return n;
		}
		
		public override Node Less(Node n)
		{
			return n;
		}

	}



}
