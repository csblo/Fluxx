﻿using System;
using System.IO;
using Fluxx.Parser;

namespace Fluxx
{
	class MainClass
	{
		
		public static int Main(string[] args)
		{
			
			var runtime = new Runtime.Runtime(Config.Default);
			
			while (true)
			{
				Console.Write(">>> ");

				var line = Console.ReadLine();
				//string line = File.ReadAllText("tests/ascii-art-box.fluxx");
				//string line = File.ReadAllText("tests/repeat.fluxx");
				
				runtime.Start(line);
			}

			return 0;
		}
	}
}
