﻿using System;
using System.IO;
using System.Reflection;
using System.Collections.Generic;
using Fluxx.Parser;
using Fluxx.Core;

namespace Fluxx.Runtime
{
    public class Runtime
    {

        private Node _tree;
        private readonly IParser _parser;
        private readonly Reducer.Reducer _reducer;
        
        //references
        private IDictionary<string, Delegate> _refs;
        
        public Runtime(Config config)
        {
            _parser = config.Parser;
            _reducer = new Reducer.Reducer();
            
            _refs = new Dictionary<string, Delegate>();
            LoadBuiltin();
        }

        private void LoadBuiltin()
        {
            //Load builtin functions
            string assemblyPath = System.IO.Path.GetDirectoryName(Assembly.GetEntryAssembly().Location);
            string path = Path.Combine(assemblyPath, "Fluxx.Builtins.dll");
            _refs = ReferenceImporter.LoadModule(path);
        }

        public void Start(string program)
        {
            _tree = _parser.Parse(program);
            
            //add builtins
            foreach (var kvRef in _refs)
            {
                var n = new Node(NodeType.Container, _tree);
                n.Value = kvRef.Value;
                //_tree.Context.AddVar(kvRef.Key, n);
                _tree.AddVar(kvRef.Key, n);
            }
            
            Run(_tree);
        }

        private void Run(Node n)
        {
            try
            {
                var reduceTree = _reducer.StartReduce(_tree);
                Console.WriteLine(reduceTree.ToString());
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                Console.WriteLine(ex.StackTrace);
            }
        }

        public void NodeValueHasChanged(Node n, object value)
        {
            //Re-execute
        }
        
        
        
    }
}