﻿using System.ComponentModel;
using System.Linq;
using System.Windows.Forms;
using Fluxx.Core;

namespace Fluxx.Builtins
{
    public static class Main
    {

        public static Node add(Node input)
        {
            Node res = new Node(NodeType.Number);
            res.Value = (double)input[0].Value + (double)input[1].Value;
            return res;
        }
        
        /*
        public static object add(object[] inputs)
        {
            return inputs.Aggregate((i,n) => (double)i + (double)n);
        }

        public static object sub(object[] inputs)
        {
            return inputs.Aggregate((i, n) => (double) i - (double)n);
        }

        public static object mul(object[] inputs)
        {
            return inputs.Aggregate((i, n) => (double) i * (double)n);
        }
        
        public static object div(object[] inputs)
        {
            return inputs.Aggregate((i, n) => (double) i / (double)n);
        }

        public static object dotProduct(object[] inputs)
        {
            return null;
        }
        */
    }
}