﻿using System;
using System.Linq;
using System.Text;

namespace Fluxx.Core
{
	public static class TreeWriter
	{
		

		public static string Write(Node n)
		{
			StringBuilder sb = new StringBuilder();

			if (!n.IsAnonymous)
				sb.AppendFormat("{0}=", n.Name);



			if (n.Type == NodeType.Blank)
			{
				sb.Append("<call>");
			}

			if (n.Type == NodeType.Container)
			{
				if (n.Value == null)
				{
					/*
					StringBuilder strVars = new StringBuilder();
					foreach (var kv in n.Context.variables)
					{
						strVars.AppendFormat("{0}={1}, ", kv.Key, TreeWriter.Write(kv.Value));
					}
					

					if (n.Children.Count() > 0)
						sb.AppendFormat("[{0}{1}]", strVars, String.Join(", ", n.Children.Select(x => TreeWriter.Write(x))));
					else
					{
						if (strVars.Length >= 2)
							strVars.Remove(strVars.Length - 2, 2);
						sb.AppendFormat("[{0}]", strVars);
					}*/
					
					if (n.Children.Count() > 0)
						sb.AppendFormat("[{0}]", String.Join(", ", n.Children.Select(x => TreeWriter.Write(x))));

				}
				else
					sb.Append("<func>");

			}
			else if (n.Type == NodeType.Id)
				sb.Append(n.Value);
			else if (n.Type == NodeType.Number)
				sb.Append(n.Value);
			else if (n.Type == NodeType.Bool)
				sb.Append(n.Value);
			else if (n.Type == NodeType.String)
				sb.AppendFormat("'{0}'",n.Value);
			else if (n.Type == NodeType.Empty)
				sb.Append("_");
			else if (n.Type == NodeType.Add)
				sb.AppendFormat("{0}+{1}", TreeWriter.Write(n.Children.ElementAt(0)), TreeWriter.Write(n.Children.ElementAt(1)));
			else if (n.Type == NodeType.Range)
				sb.AppendFormat("{0}..{1}", TreeWriter.Write(n.Children.ElementAt(0)), TreeWriter.Write(n.Children.ElementAt(1)));

			return sb.ToString();
		}
	}
}
