﻿using System;
using System.Linq;
using System.Collections.Generic;

namespace Fluxx.Core
{
	public class Context
	{
		
		public List<Node> expressions;
		public Dictionary<string, Node> variables;

		public Context()
		{
			expressions = new List<Node>();
			variables = new Dictionary<string, Node>();
		}

		public bool AddVar(Node n)
		{
			return AddVar(n.Name, n);
		}
		
		public bool AddVar(string name, Node n)
		{
			if (!variables.ContainsKey(name))
			{
				variables.Add(name, n);
				return true;
			}

			return false;
		}

		public void RemoveVar(string name)
		{
			if (variables.ContainsKey(name))
				variables.Remove(name);
		}

		public Node GetVar(string name)
		{
			if (variables.ContainsKey(name))
				return variables[name];
			else
				return null;
		}

		public IEnumerable<Node> GetExpressions()
		{
			return expressions.AsEnumerable();
		}

		public Node GetExpression(int index)
		{			
			return expressions[index];
		}

		public int CountExpression()
		{
			return expressions.Count;
		}

		public void AddFromContext(Context c)
		{
			this.expressions.AddRange(c.expressions);

			foreach (var kv in c.variables)
				this.variables.Add(kv.Key, kv.Value);
		}

	}
}
