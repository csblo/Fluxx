﻿namespace Fluxx.Parser
{
    public struct BinarySplitObject
    {
        public string Left { get; set; }
        public string Right { get; set; }
        public int Index { get; set; }
        public int Length { get; set; }
        public Symbol Separator { get; set; }
    }
}