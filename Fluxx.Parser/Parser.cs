﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using Fluxx.Core;

namespace Fluxx.Parser
{
	public sealed class Parser : IParser
	{

		public Config Config { get; }
		public Preprocessor Preprocessor { get; }
		public ParserHelper ParserHelper { get; }

		private Node _root;
		private Node _curScope;
		
		internal Parser(Config config)
		{
			Config = config;
			Preprocessor = new Preprocessor(config);
			ParserHelper = new ParserHelper(config);
		}

		public Node Parse(string program)
		{
			var minifyProgram = Preprocessor.Minify(program);
			
			var root = new Node(NodeType.Container);
			_curScope = root;
			_root = root;
			root.Name = "root";
			root.AddRange(ParseDeclarations(minifyProgram));
			root.Text = minifyProgram;
			return root;
		}

		private IEnumerable<Node> ParseDeclarations(string expression)
		{
			var declarations = ParserHelper.SplitDeclarations(expression).Where(d => d != String.Empty);

			var parsedDeclarations = declarations.Select(TryParseDeclaration);
			//Exclude binded node which are named
			return parsedDeclarations.Where(cn => cn.IsAnonymous).AsEnumerable();
		}

		private Node TryParseDeclaration(string expression) =>
				expression == String.Empty ? null :
				TryParseContainers(expression) ?? 
			    TryParseStatement(expression) ??
				TryParseOperators(expression) ??
				TryParseTerminal(expression);

		private Node TryParseContainers(string expression) => 
			Config.Containers.Select(c => TryParseContainer(c, expression)).FirstOrDefault(n => n != null);

		private Node TryParseOperators(string expression) =>
			Config.Precedences.Select(p => TryParseOperator(p.Symbols, expression)).FirstOrDefault(n => n != null);

		private Node TryParseContainer(Symbol symbol, string expression)
		{
			if (!ParserHelper.IsSurroundBy(expression, symbol))
				return null;

			var cleanExpression = expression.Remove(expression.Length - 1, 1).Remove(0, 1).Trim();

			var n = new Node(symbol.NodeType, _curScope);

			
			var lastScope = _curScope;
			
			if (n.IsContainer)
				_curScope = n;
			
			n.Text = expression;
			n.AddRange(ParseDeclarations(cleanExpression));

			_curScope = lastScope;

			return n;
		}

		private Node TryParseStatement(string expression)
		{
			foreach (var rule in Config.Rules)
			{
				var matches = rule.Match(expression);
				if (matches != null)
				{
					var type = (NodeType)Enum.Parse(typeof(NodeType), rule.Lexeme.ToUpper());
					var n = new Node(type, _curScope);

					foreach (var match in matches)
					{
						n.Add(TryParseDeclaration(match));
					}

					return n;
				}
			}
			
			return null;
		}

		private Node TryParseTerminal(string expression)
		{
			//Terminal symbols
			foreach (var symbol in Config.Terminals)
			{
				var regex = new Regex($"^{symbol.Regex}$");
				var m = regex.Match(expression);
				if (!m.Success)
					continue;

				object val = expression;

				//Convert type of value for node
				if (Config.Converts.ContainsKey(symbol.NodeType))
					val = Config.Converts[symbol.NodeType](expression);

				//string ?
				if (symbol.NodeType == NodeType.String)
					val = ((string)val).Remove(expression.Length - 1, 1).Remove(0, 1);

				var n = new Node(symbol.NodeType, _curScope);
				n.Text = expression;
				n.Value = val;
				return n;

			}

			return null;
		}
		
		private Node TryParseOperator(IEnumerable<Symbol> s, string expression)
		{
			var splits = ParserHelper.BinarySplits(expression, s);

			if (splits.Count == 0)
				return null;

			//We consider that operator with the same precedence have always the same associativity ? 
			//Indeed it may cause some ambuiguities if it's not the case
			var assoc = s.First().Assoc;

			//Search for a consistant binary split, 
			//look for ambuiguous operators like minus, composed operator like ==, etc...
			var nullableBs = FindConsistantSO(splits, assoc);

			if (nullableBs == null)
				return null;

			var bs = nullableBs.Value;
			
			var n = new Node(bs.Separator.NodeType, _curScope);
			n.Text = expression;

			if (bs.Separator.NodeType == NodeType.Bind)
				return ParseBinding(bs);

			if (bs.Separator.Type == SymbolType.BinOp)
				return ParseBinaryOperator(bs);

			if (bs.Separator.Type == SymbolType.UnaOp)
				return ParseUnaryOperator(bs);

			return null;
		}

		private Node ParseBinding(BinarySplitObject bs)
		{
			var lExp = TryParseDeclaration(bs.Left);
			var rExp = TryParseDeclaration(bs.Right);
			
			var name = lExp.Value.ToString();
			var cpy = rExp.ShallowCopy();
			cpy.Name = name;
			var added = _curScope.AddVar(cpy);
			
			
			if (!added)
				throw new Exception($"parse error - immutable {name} is already binded");
			
			return cpy;
		}
		
		private Node ParseBinaryOperator(BinarySplitObject bs)
		{
			var n = new Node(bs.Separator.NodeType, _curScope);
			
			var lExp = TryParseDeclaration(bs.Left);
			var rExp = TryParseDeclaration(bs.Right);
				
			n.Add(lExp);
			n.Add(rExp);
			
			return n.Children.Count() == 2 ? n : null;
		}

		private Node ParseUnaryOperator(BinarySplitObject bs)
		{
			var n = new Node(bs.Separator.NodeType, _curScope);
			
			if (bs.Separator.Assoc == Assoc.Left)
				n.Add(TryParseDeclaration(bs.Left));
			else
				n.Add(TryParseDeclaration(bs.Right));
			
			return n.Children.Count() == 1 ? n : null;
		}

		private BinarySplitObject? FindConsistantSO(IEnumerable<BinarySplitObject> splits, Assoc assoc)
		{
			var i = assoc == Assoc.Left ? splits.Count() - 1 : 0;

			bool 	isConsistentSplit = true, 
					isConsistentLeft = true, 
					isConsistentRight = true;

			BinarySplitObject bs;
			
			do
			{
				bs = splits.ElementAt(i);

				var lsplits = ParserHelper.BinarySplits(bs.Left, Config.Operators).Where(x => x.Index + x.Length == bs.Index).ToList();
				var rsplits = ParserHelper.BinarySplits(bs.Right, Config.Operators).Where(x => x.Index == 0).ToList();
				
				
				/*
				isConsistentSplit = (bs.Separator.Type == SymbolType.BINARY_OPERATOR && bs.Left != String.Empty &&
				                          bs.Right != String.Empty) ||
				                         (bs.Separator.Type == SymbolType.UNARY_OPERATOR && bs.Separator.Assoc == Assoc.RIGHT &&
				                          bs.Right != String.Empty) ||
				                         (bs.Separator.Type == SymbolType.UNARY_OPERATOR && bs.Separator.Assoc == Assoc.LEFT &&
				                          bs.Left != String.Empty);
				*/
				
				isConsistentLeft = lsplits.Count == 0 ||
					lsplits.Any(x => x.Separator.Type == SymbolType.UnaOp && x.Separator.Assoc == Assoc.Left);
				isConsistentRight = rsplits.Count == 0 ||
					rsplits.Any(x => x.Separator.Type == SymbolType.UnaOp && x.Separator.Assoc == Assoc.Right);
				
				i += (int) assoc;
				
			} while (i >= 0 && i < splits.Count() && !(/*isConsistentSplit && */isConsistentLeft && isConsistentRight));
			
			if (/*isConsistentSplit && */isConsistentLeft && isConsistentRight)
				return bs;

			return null;
		}


	}
	
}
