﻿using System;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;

namespace Fluxx.Parser
{
    public class Preprocessor
    {
        private readonly Config conf;
        
        public Preprocessor(Config conf)
        {
            this.conf = conf;
        }
        
        public string Minify(string program)
        {
            var s = program;
            
            s = RemoveComments(s);
            s = RemoveLineBreaks(s);
            s = RemoveMultiSpaces(s);
            s = RemoveSpacesAroundComma(s);
            s = RemoveSpacesAroundContainers(s);
            s = RemoveSpacesAroundOperators(s);

            return s.Trim();
        }

        private string RemoveComments(string s)
        {
            var sb = new StringBuilder();
            
            using (StringReader sr = new StringReader(s))
            {
                string line;
                while ((line = sr.ReadLine()) != null)
                {
                    if (!line.StartsWith("#"))
                        sb.AppendLine(line);
                }    
            }
            
            return sb.ToString();
        }
        
        private string RemoveLineBreaks(string s)
        {
            s = s.Replace("\r", "");
            s = s.Replace("\n", "");
            return s;
        }

        private string RemoveMultiSpaces(string s)
        {
            return Regex.Replace(s, @"[ \t]+", " ");
        }

        private string RemoveSpacesAroundComma(string s)
        {
            return Regex.Replace(s, @"[ \t]*\,[ \t]*", ",");
        }

        private string RemoveSpacesAroundContainers(string s)
        {
            foreach (var container in conf.Containers)
            {
                string regex1 = String.Format(@"\{0}[ \t]*", container.Left);
                s = Regex.Replace(s, regex1, container.Left);
                
                string regex2 = String.Format(@"[ \t]*\{0}", container.Right);
                s = Regex.Replace(s, regex2, container.Right);
            }

            return s;
        }

        private string RemoveSpacesAroundOperators(string s)
        {
            //Remove space around operators
            /*
            foreach (var op in operators)
            {
                string regex = String.Format(@"[ \t]*{0}[ \t]*", op.Regex);
                s = Regex.Replace(s, regex, "")
            }
            */
            return s;
        }
        
    }
}