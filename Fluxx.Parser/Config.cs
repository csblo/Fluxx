﻿using System;
using System.Globalization;
using System.Linq;
using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;
using Fluxx.Core;

namespace Fluxx.Parser
{
    public class Config
    {
        
        public IEnumerable<Symbol> Operators { get; private set; }
        public IEnumerable<Symbol> Containers { get; private set; }
        public IEnumerable<Symbol> Terminals { get; private set; }
        public IEnumerable<GrammarRule> Rules { get; private set; }
        public IEnumerable<PrecedenceGroup> Precedences { get; private set; }
        
        public Dictionary<NodeType, Func<string, object>> Converts { get; private set; }

        private static Config _defaultConfig;

	    public static Config Default =>
		    _defaultConfig ?? (_defaultConfig = Create("config"));

	    public IParser Parser { get; }
	    
	    private Config()
	    {
		    Parser = new Parser(this);
	    }
	    
        public static Config Create(string directory)
        {
	        var config = new Config
	        {
		        Converts = GetConverts(),
		        Operators = LoadOperators(Path.Combine(directory, "operators.txt")),
		        Containers = LoadContainers(Path.Combine(directory, "containers.txt")),
		        Terminals = LoadTerminals(Path.Combine(directory, "terminals.txt")),
	        };
            
	        //
            config.Rules = LoadRules(Path.Combine(directory, "grammar.txt"), config.Parser.ParserHelper);

	        //Group operators symbols by precedence
            config.Precedences = config.Operators.GroupBy(s => s.Precedence).Select(g => new PrecedenceGroup(g.Key, g.ToArray())).ToList();
            
            return config;
        }
	    
	    /// <summary>
	    /// Load operator symbols from config file
	    /// </summary>
	    /// <param name="path">Path of the config file</param>
	    /// <returns>A list of symbol</returns>
		private static IEnumerable<Symbol> LoadOperators(string path) =>
			
			ReadFile(path, (parts) =>
				new Symbol() 
				{
					Lexeme = parts[0],
					Regex = parts[2],
					CompiledRegex = new Regex(parts[2], RegexOptions.Compiled),
					Assoc = (Assoc)Enum.Parse(typeof(Assoc), parts[3]),
					Precedence = Convert.ToInt32(parts[4]),
					Type = (SymbolType)Enum.Parse(typeof(SymbolType), parts[5]),
					NodeType = (NodeType)Enum.Parse(typeof(NodeType), parts[1])
				});

	    /// <summary>
	    /// Load container symbols from config file
	    /// </summary>
	    /// <param name="path">Path of the config file</param>
	    /// <returns>A list of symbol</returns>
	    private static IEnumerable<Symbol> LoadContainers(string path) =>
		    
		    ReadFile(path, (parts) =>
			    new Symbol()
			    {
				    NodeType = (NodeType) Enum.Parse(typeof(NodeType), parts[0]),
				    Lexeme = parts[0],
				    Left = parts[1],
				    Right = parts[2],
				    Type = SymbolType.Container
			    });

		/// <summary>
		/// Load terminal symbols from config file
		/// </summary>
		/// <param name="path">Path of the config file</param>
		/// <returns>A list of symbol</returns>
		private static IEnumerable<Symbol> LoadTerminals(string path) =>
			
			ReadFile(path, (parts) =>
				new Symbol()
				{
					Lexeme = parts[0],
					Regex = parts[1],
					NodeType = (NodeType)Enum.Parse(typeof(NodeType), parts[0]),
					Type = SymbolType.Terminal
				});

	    /// <summary>
	    /// Load grammar rules from config file
	    /// </summary>
	    /// <param name="path">Path of the config file</param>
	    /// <param name="parserHelper">The parser helper</param>
	    /// <returns>A collection of grammar rules</returns>
		private static IEnumerable<GrammarRule> LoadRules(string path, ParserHelper parserHelper)
		{
			return ReadFile(path, (parts) =>
				new GrammarRule(parts.ElementAt(0), parts.ElementAt(1), parserHelper));
		}

	    /// <summary>
	    /// General abstract function that read a file and split components
	    /// separated by a ';' in order to mapping each lines to an object
	    /// </summary>
	    /// <param name="path">Path of the file to read</param>
	    /// <param name="f">Function that return components by lines</param>
	    /// <typeparam name="T">The type of object expected as output of the function that map components to</typeparam>
	    /// <returns>A collection of objects mapped from the file of type T</returns>
		private static IEnumerable<T> ReadFile<T>(string path, Func<string[], T> f)
		{
			var collection = new List<T>();

			using (var sr = new StreamReader(path))
			{
				var line = String.Empty;
				while ((line = sr.ReadLine()) != null)
				{
					var parts = line.Split(';').Select(s => s.Trim()).ToArray();
					collection.Add(f(parts));
				}
			}

			return collection;
		}

	    /// <summary>
	    /// Get the conversion functions by type of node
	    /// </summary>
	    /// <returns>A dictionary</returns>
		private static Dictionary<NodeType, Func<string, object>> GetConverts()
		{
			return new Dictionary<NodeType, Func<string, object>>
			{
				{NodeType.Number, (s) => Convert.ToDouble(s, new NumberFormatInfo { NumberDecimalSeparator = "." })},
				{NodeType.Bool, (s) => Convert.ToBoolean(s)}
			};
		}
        
    }
}